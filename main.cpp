#include "CubeSelectorCtrl.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    CubeSelectorCtrl w;

    w.show();

    w.SetParentDimentions(500,600,500);
    w.SetChildDimentions(30,40,50,100,200,300);
    w.Initialize();

    return a.exec();
}
