#include "Util.h"
#include <math.h>
Util::Util()
{
}

void Util::SetManulaSpinValue(QDoubleSpinBox *pSpinBox, double dValue)
{
    pSpinBox->blockSignals(true);
    pSpinBox->setValue(dValue);
    pSpinBox->blockSignals(false);
}

bool Util::IsEqual(double dValue1, double dValue2)
{
    return (fabs(dValue1 - dValue2) <= DBL_EPSILON);
}
