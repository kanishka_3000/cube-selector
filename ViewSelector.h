#ifndef VIEWSELECTOR_H
#define VIEWSELECTOR_H

#include <QWidget>
#include <QListWidget>
#include <QStackedWidget>
#include <QButtonGroup>
namespace Ui {
class ViewSelector;
}

class ViewSelector : public QWidget
{
    Q_OBJECT

public:
    explicit ViewSelector(QWidget *parent = 0);
    ~ViewSelector();
    QStackedWidget* GetStack();
public slots:
    void OnButtonClicked(QAbstractButton * button);
    void OnTopSelected();
    void OnSideSelected();
private:
    Ui::ViewSelector *ui;

};

#endif // VIEWSELECTOR_H
