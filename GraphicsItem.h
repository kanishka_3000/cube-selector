#ifndef GRAPHICSITEM_H
#define GRAPHICSITEM_H
#include <QGraphicsRectItem>
#include <QGraphicsSceneMouseEvent>
enum
{
    boxW = 10,
    boxH = 10
};
class CubeSelector;
class GraphicsItem: public QGraphicsRectItem
{
public:
    GraphicsItem(const QRectF & rect, QGraphicsItem * parent,CubeSelector* pCtrl);
     void paint(QPainter * painter, const QStyleOptionGraphicsItem * option, QWidget * widget);


protected:
     bool IsOnActiveCorner(QPointF oPoint);
     virtual void	mousePressEvent(QGraphicsSceneMouseEvent * event);
     virtual void	mouseReleaseEvent(QGraphicsSceneMouseEvent * event);
     virtual QVariant itemChange(GraphicsItemChange change, const QVariant &value);
private:
     bool b_Activated;
     QPointF o_PrevPos;

     CubeSelector* p_Parent;
};

#endif // GRAPHICSITEM_H
