#include "ManualCtrl.h"
#include "ui_ManualCtrl.h"
#include <Util.h>
ManualCtrl::ManualCtrl(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ManualCtrl),p_Model(NULL)
{
    ui->setupUi(this);
}

ManualCtrl::~ManualCtrl()
{
    delete ui;
}

void ManualCtrl::SetModel(CubeModel *pModel)
{
    p_Model = pModel;

}

void ManualCtrl::Initialize()
{
    Util::SetManulaSpinValue(ui->spin_X,p_Model->GetValue(Child,X_));
    Util::SetManulaSpinValue(ui->spin_Y,p_Model->GetValue(Child,Y_));
    Util::SetManulaSpinValue(ui->spin_Z,p_Model->GetValue(Child,Z_));

    Util::SetManulaSpinValue(ui->spin_Width,p_Model->GetValue(Child,W_));
    Util::SetManulaSpinValue(ui->spin_Height,p_Model->GetValue(Child,H_));
    Util::SetManulaSpinValue(ui->spin_Depth,p_Model->GetValue(Child,D_));
}

QDoubleSpinBox *ManualCtrl::GetSpinBox(int iBox)
{
    switch (iBox) {
    case X_:
    {
        return ui->spin_X;
        break;
    }
    case Y_:
    {
        return ui->spin_Y;
        break;
    }
    case Z_:
    {
        return ui->spin_Z;
        break;
    }
    case W_:
    {
        return ui->spin_Width;
        break;
    }
    case H_:
    {
        return ui->spin_Height;
        break;
    }
    case D_:
    {
        return ui->spin_Depth;
        break;
    }
    default:
    {
     return NULL;
        break;
    }
    }
}
