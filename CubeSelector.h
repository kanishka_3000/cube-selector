#ifndef CUBESELECTOR_H
#define CUBESELECTOR_H

#include <QWidget>
#include <ui_CubeSelector.h>
#include <QGraphicsScene>
#include <QDoubleSpinBox>
#include <QGraphicsTextItem>
#include <CubeModel.h>
class CubeSelector : public QWidget, Ui::CubeSelector
{
    Q_OBJECT

public:
    explicit CubeSelector(QWidget *parent = 0);
    ~CubeSelector();

    void OnRectResized(QRectF oRect);
    void OnRectMoved(QRectF oRect);
    void RegisterHorizontal(QDoubleSpinBox* pSpinBox, QString sText,int iIdnetifier);
    void RegisterVertical(QDoubleSpinBox* pSpinBox, QString sText, int iIdentifier);

    void RegisterHorizontalSize(QDoubleSpinBox* pSinBox, QString sText,int iIdentifier);
    void RegisterVerticalSize(QDoubleSpinBox* pSpinBox, QString sText, int iIdentifier);

    void RegistrationComplete();
    void SetModel(CubeModel* pModel){p_Model = pModel;}
signals:
    //Connect when internal rectangle resize notifications are needed
    void NotifyRectResized(QSizeF oRect);
    //Connect whhen internal rectangle move notification needed
    void NotifyRectMoved(QRectF oRect);

    //Does not have any graphical representation change, only mapping of
    //spin box to signal >>>
    void NotifyZChanged(double dValue);
    void NotifyDepthChanged(double dValue);
    //<<<

public slots:
    void OnXSpinBox(double dValue);
    void OnYSpinBox(double dValue);
    void OnZSpinBox(double dValue);

    void OnWidthSpinBox(double dValue);
    void OnHeightSpinBox(double dValue);
    void OnDepthSpinBox(double dValue);


private:

    void ResetText();
    QGraphicsScene o_Scene;
    QGraphicsRectItem* p_Rect;
    QDoubleSpinBox* spin_H;
    QDoubleSpinBox* spin_V;

    QDoubleSpinBox* spin_HS;
    QDoubleSpinBox* spin_VS;

    QGraphicsTextItem o_HorizontalText;
    QGraphicsTextItem o_VerticalTextText;

    QGraphicsTextItem o_HPointText;
    QGraphicsTextItem o_VPointText;

    QString s_HText;
    QString s_VText;

    QString s_PHText;
    QString s_PVText;

    int i_HIdentifier;
    int i_VIdentifier;
    int i_HSIdentifier;
    int i_VSIdentifier;
    bool b_Lock;

    CubeModel* p_Model;

};

#endif // CUBESELECTOR_H
