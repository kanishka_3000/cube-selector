#-------------------------------------------------
#
# Project created by QtCreator 2015-07-18T19:35:38
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = CubeSelector
TEMPLATE = app


SOURCES += main.cpp\
    GraphicsItem.cpp \
    CubeSelector.cpp \
    ManualCtrl.cpp \
    Util.cpp \
    ViewSelector.cpp \
    CubeSelectorCtrl.cpp \
    CubeModel.cpp

HEADERS  += CubeSelector.h \
    GraphicsItem.h \
    ManualCtrl.h \
    Util.h \
    ViewSelector.h \
    CubeSelectorCtrl.h \
    Defs.h \
    CubeModel.h

FORMS    += CubeSelector.ui \
    ManualCtrl.ui \
    ViewSelector.ui \
    CubeSelectorCtrl.ui

RESOURCES += \
    CubeSelector.qrc
