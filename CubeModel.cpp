#include "CubeModel.h"
#include <Defs.h>
#include <QDebug>
#include <iostream>
#include <Util.h>
#include <assert.h>
CubeModel::CubeModel(QObject *parent) :
    QObject(parent)
{
    for(int i = 0; i <= MAX_DEF; i++)
    {
        ad_ChildValues[i] = -1;
        ad_ParentValues[i] = -1;
    }
    set_ComparativeFields.insert(W_);
    set_ComparativeFields.insert(H_);
    set_ComparativeFields.insert(D_);
}



void CubeModel::_OnChange(int iType, double dValue)
{
    if(iType > MAX_DEF || iType <= 0)
    {
        assert(false);
        return;
    }

    if(!qFuzzyCompare(dValue,ad_ChildValues[iType]))
    {
         qDebug() << "OnChange Input"<< iType << "  " << dValue;
         if(Util::IsEqual(dValue,0.0))
         {
             qDebug() << "Size can not be 0";
             return;
         }
        ad_ChildValues[iType] = dValue;
        if(set_ComparativeFields.contains(iType) )
        {
            qDebug() << "Conversion" << dValue << " " << ad_ParentValues[iType];
            dValue = dValue/ad_ParentValues[iType];
        }
        qDebug() << "Notify with converstion"<< iType << "  " << dValue;
        NotifyValueChanged(iType, dValue);
    }


}

double CubeModel::GetValue(Container eContainer, int iDimention)
{
    if(iDimention > MAX_DEF || iDimention <= 0)
    {
        assert(false);
        return 0;
    }
    switch (eContainer)
    {
        case Parent:
        {
            return ad_ParentValues[iDimention];
            break;
        }
        case Child:
        {
            return ad_ChildValues[iDimention];
            break;
        }
    }
    return -1;
}

void CubeModel::SetValue(Container eContainer, int iDimention, double dValue)
{
    if(iDimention > MAX_DEF || iDimention <= 0)
    {
        assert(false);
        return ;
    }
    if(set_ComparativeFields.contains(iDimention) && Util::IsEqual(dValue,0.0))
    {
        assert(false && "This field can not be 0");
    }
    switch (eContainer)
    {
        case Parent:
        {
             ad_ParentValues[iDimention] = dValue;
            break;
        }
        case Child:
        {
             ad_ChildValues[iDimention] = dValue;
            break;
        }
    }
}
