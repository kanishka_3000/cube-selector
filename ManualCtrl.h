#ifndef MANUALCTRL_H
#define MANUALCTRL_H

#include <QWidget>
#include <QDoubleSpinBox>
#include <Defs.h>
#include <CubeModel.h>
namespace Ui {
class ManualCtrl;
}

class ManualCtrl : public QWidget
{
    Q_OBJECT

public:
    explicit ManualCtrl(QWidget *parent = 0);
    ~ManualCtrl();
    void SetModel(CubeModel* pModel);
    void Initialize();
    QDoubleSpinBox* GetSpinBox(int iBox);
private:

    Ui::ManualCtrl *ui;
    CubeModel* p_Model;
};

#endif // MANUALCTRL_H
