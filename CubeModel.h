#ifndef CUBEMODEL_H
#define CUBEMODEL_H

#include <QObject>
#include <QMap>
#include <QSet>
#define DIMENSIONS  7
enum Container
{
    Parent,
    Child
};

class CubeModel : public QObject
{
    Q_OBJECT
public:
    explicit CubeModel(QObject *parent = 0);

    void _OnChange(int iType, double dValue);

    ///Value retrieval from the model.
    double GetValue(Container eContainer, int iDimention);
    ///Set values of the model
    void SetValue(Container eContainer, int iDimention, double dValue);
signals:
    ///Public notification signal for external party to subscribe into>>>
    void NotifyValueChanged(int iType, double dValue);
    ///<<<<
public slots:

private:
    double ad_ChildValues[DIMENSIONS];
    double ad_ParentValues[DIMENSIONS];
    QSet<int> set_ComparativeFields;
};

#endif // CUBEMODEL_H
