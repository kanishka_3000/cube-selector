#ifndef UTIL_H
#define UTIL_H
#include <QDoubleSpinBox>
#define DBL_EPSILON 0.0000001
class Util
{
public:
    Util();

    static void SetManulaSpinValue(QDoubleSpinBox* pSpinBox, double dValue);
    static bool IsEqual(double dValue1, double dValue2);
};

#endif // UTIL_H
