#include "GraphicsItem.h"
#include <QPainter>
#include <QDebug>
#include <CubeSelector.h>
GraphicsItem::GraphicsItem(const QRectF & rect, QGraphicsItem * parent, CubeSelector *pCtrl)
    :QGraphicsRectItem(rect,parent),b_Activated(false),p_Parent(pCtrl)
{
}

void GraphicsItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    if (isSelected())
    {
        const QRectF bbox(this->boundingRect());
        const qreal x = bbox.x();
        const qreal y = bbox.y();
        const qreal w = bbox.width();
        const qreal h = bbox.height();

        painter->fillRect(QRectF(x + w - boxW, y + h - boxH, boxW, boxH), Qt::darkGray);

    }
    QGraphicsRectItem::paint(painter,option,widget);
}

bool GraphicsItem::IsOnActiveCorner(QPointF oPoint)
{
    const QRectF bbox(this->boundingRect());
    const qreal x = bbox.x();
    const qreal y = bbox.y();
    const qreal w = bbox.width();
    const qreal h = bbox.height();

    QRectF oRightBottom = QRectF(x + w - boxW, y + h - boxH, boxW, boxH);
    return oRightBottom.contains(oPoint);
}

void GraphicsItem::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    QPointF oPos = event->pos();
    if(IsOnActiveCorner(oPos))
    {
        b_Activated = true;
        o_PrevPos = oPos;
        setFlag(QGraphicsItem::ItemIsMovable,false);
    }
    QGraphicsRectItem::mousePressEvent(event);
}

void GraphicsItem::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsRectItem::mouseReleaseEvent(event);
    if(b_Activated)
    {
        b_Activated = false;
        QPointF oCurrentPoint = event->pos();
        const QPointF shift(oCurrentPoint.x() - o_PrevPos.x(), oCurrentPoint.y() - o_PrevPos.y());
        QRectF itemRect(rect());
        qDebug() << itemRect;
        itemRect.setBottomRight(itemRect.bottomRight()+ shift);
        setRect(itemRect);
        p_Parent->OnRectResized(itemRect);
        qDebug() << itemRect;

        setFlag(QGraphicsItem::ItemIsMovable,true);
    }
    else
    {
        p_Parent->OnRectMoved(sceneBoundingRect());
    }

}

QVariant GraphicsItem::itemChange(QGraphicsItem::GraphicsItemChange change, const QVariant &value)
{

    if(change == ItemPositionChange )
    {
       // qDebug() << "Item Moved";
        QPointF newPos = value.toPointF();
        QRectF oParent = parentItem()->boundingRect();
        QRectF oChild = boundingRect();
        if(!oParent.contains(oChild))
        {
            newPos.setX(0);
            newPos.setY(0);
            return newPos;
        }
    }
    return QGraphicsRectItem::itemChange(change,value);
}
