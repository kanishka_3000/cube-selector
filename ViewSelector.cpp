#include "ViewSelector.h"
#include "ui_ViewSelector.h"

ViewSelector::ViewSelector(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ViewSelector)
{
    ui->setupUi(this);

}

ViewSelector::~ViewSelector()
{
    delete ui;
}

QStackedWidget *ViewSelector::GetStack()
{
    return ui->p_Side;
}

void ViewSelector::OnButtonClicked(QAbstractButton *button)
{
    if(button == ui->chk_Top)
    {
        OnTopSelected();
    }
    else
    {
        OnSideSelected();
    }
}

void ViewSelector::OnTopSelected()
{
    ui->p_Side->setCurrentIndex(0);
}

void ViewSelector::OnSideSelected()
{
     ui->p_Side->setCurrentIndex(1);
}
