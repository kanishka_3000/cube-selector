#ifndef CUBESELECTORCTRL_H
#define CUBESELECTORCTRL_H

#include <QWidget>
#include <CubeSelector.h>
#include <CubeModel.h>
namespace Ui {
class CubeSelectorCtrl;
}

class CubeSelectorCtrl : public QWidget
{
    Q_OBJECT

public:
    explicit CubeSelectorCtrl(QWidget *parent = 0);
    ~CubeSelectorCtrl();
    void CreateCtrl();

    ///>>> Public interface functions
    /// First function to call before initialization
    /// Sets dimentions of parent cube, no x,y,z provided since inherantly 0
    /// Failure will assert
    void SetParentDimentions(double dWidth, double dHeight, double dDepth);
    ///Should be called after parentDimentions function and before Initlization
    /// Sets Data of the internal cube, Failures would assert
    void SetChildDimentions(double dX, double dY, double dZ,
                            double dWidth, double dHeight, double dDepth);
    ///Initlize data model and set values extracted from the model.
    void Initialize();

    ///<<<
    ///Returns the cube model, if external party needs to subscribe to changes
    /// Public signals for change from the model can be used.
    CubeModel* GetModel(){return &o_Model;}
private:
    Ui::CubeSelectorCtrl *ui;
    CubeSelector o_TopView;
    CubeSelector o_SideView;
    CubeModel o_Model;

    bool b_ParentSet;
    bool b_ChildSet;
};

#endif // CUBESELECTORCTRL_H
