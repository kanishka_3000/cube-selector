#include "CubeSelectorCtrl.h"
#include <ui_CubeSelectorCtrl.h>
#include <ViewSelector.h>
#include <Defs.h>
#include <assert.h>
CubeSelectorCtrl::CubeSelectorCtrl(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::CubeSelectorCtrl),b_ParentSet(false),b_ChildSet(false)
{
    ui->setupUi(this);
    connect(ui->p_ViewSelector->GetStack(),SIGNAL(currentChanged(int)),
            ui->p_Selectors,SLOT(setCurrentIndex(int)) );

    ui->p_TopSelector->SetModel(&o_Model);
    ui->p_SideSelector->SetModel(&o_Model);
    ui->p_ManualCtrl->SetModel(&o_Model);

}
void CubeSelectorCtrl::Initialize()
{
    if(!b_ChildSet || !b_ParentSet)
    {
        //Set Parent and child dimentions fisrt
        assert(false && "Set Parent and child dimentions fisrt");
    }
    ui->p_TopSelector->RegisterHorizontal(ui->p_ManualCtrl->GetSpinBox(X_),"X",X_);
    ui->p_TopSelector->RegisterVertical(ui->p_ManualCtrl->GetSpinBox(Y_),"Y",Y_);

    ui->p_SideSelector->RegisterHorizontal(ui->p_ManualCtrl->GetSpinBox(X_),"X",X_);
    ui->p_SideSelector->RegisterVertical(ui->p_ManualCtrl->GetSpinBox(Z_),"Z",Z_);

    ui->p_TopSelector->RegisterHorizontalSize(ui->p_ManualCtrl->GetSpinBox(W_),"Width",W_);
    ui->p_TopSelector->RegisterVerticalSize(ui->p_ManualCtrl->GetSpinBox(H_), "Height",H_);

    ui->p_SideSelector->RegisterHorizontalSize(ui->p_ManualCtrl->GetSpinBox(W_),"Width",W_);
    ui->p_SideSelector->RegisterVerticalSize(ui->p_ManualCtrl->GetSpinBox(D_),"Depth",D_);

    ui->p_TopSelector->RegistrationComplete();
    ui->p_SideSelector->RegistrationComplete();

    ui->p_ManualCtrl->Initialize();
}

void CubeSelectorCtrl::SetParentDimentions(double dWidth, double dHeight, double dDepth)
{
    o_Model.SetValue(Parent,W_,dWidth);
    o_Model.SetValue(Parent,D_,dDepth);
    o_Model.SetValue(Parent,H_,dHeight);
    b_ParentSet = true;
}

void CubeSelectorCtrl::SetChildDimentions(double dX, double dY, double dZ,
                                          double dWidth, double dHeight, double dDepth)
{
    if(!b_ParentSet)
    {
        //call setParentDimentions first
       assert(false && "call setParentDimentions first");
    }
    o_Model.SetValue(Child,X_,dX);
    o_Model.SetValue(Child,Y_,dY);
    o_Model.SetValue(Child,Z_,dZ);
    o_Model.SetValue(Child,W_,dWidth);
    o_Model.SetValue(Child,H_,dHeight);
    o_Model.SetValue(Child,D_,dDepth);
    b_ChildSet = true;
}



CubeSelectorCtrl::~CubeSelectorCtrl()
{
    delete ui;
}

void CubeSelectorCtrl::CreateCtrl()
{


}
