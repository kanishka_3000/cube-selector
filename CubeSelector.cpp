#include "CubeSelector.h"
#include <GraphicsItem.h>
#include <QDebug>
#include <Util.h>
CubeSelector::CubeSelector(QWidget *parent) :
    QWidget(parent),p_Rect(NULL),spin_H(NULL),spin_V(NULL),
    spin_HS(NULL),spin_VS(NULL),i_HIdentifier(-1),i_VIdentifier(-1),
    i_HSIdentifier(-1),i_VSIdentifier(-1),b_Lock(false),p_Model(NULL)

{
    setupUi(this);
    p_View->setScene(&o_Scene);
    p_View->setSceneRect(o_Scene.sceneRect());
    //Initialize(QRectF(0,0,800,800),100,QRectF(100,100,150,200),10,20,Qt::red);

}

CubeSelector::~CubeSelector()
{

}

void CubeSelector::OnRectResized(QRectF oRect)
{
    qDebug() << "Resized" << oRect;
    QSize oSize(oRect.height(),oRect.width());
    qDebug() << "Resized" << oSize;

    b_Lock = true;
    spin_HS->setValue(oRect.width());
    spin_VS->setValue(oRect.height());
    b_Lock = false;

    ResetText();
    p_Model->_OnChange(i_HSIdentifier,oRect.width());
    p_Model->_OnChange(i_VSIdentifier,oRect.height());
    emit NotifyRectResized(oSize);

}

void CubeSelector::OnRectMoved(QRectF oRect)
{
    qDebug() << "Moved" << oRect;
    b_Lock = true;
    spin_H->setValue(oRect.x());
    spin_V->setValue(oRect.y());
//        Util::SetManulaSpinValue(spin_H,oRect.x());
//        Util::SetManulaSpinValue(spin_V,oRect.y());
    b_Lock = false;
    ResetText();

    p_Model->_OnChange(i_HIdentifier, oRect.x());
    p_Model->_OnChange(i_VIdentifier, oRect.y());
    emit NotifyRectMoved(oRect);
}

void CubeSelector::RegisterHorizontal(QDoubleSpinBox *pSpinBox, QString sText, int iIdnetifier)
{
    spin_H = pSpinBox;
    connect(spin_H,SIGNAL(valueChanged(double)),
            this,SLOT(OnXSpinBox(double)));
    s_PHText = sText;
    i_HIdentifier = iIdnetifier;
}

void CubeSelector::RegisterVertical(QDoubleSpinBox *pSpinBox, QString sText, int iIdentifier)
{
    spin_V = pSpinBox;
    connect(spin_V,SIGNAL(valueChanged(double)),
            this,SLOT(OnYSpinBox(double)));

    s_PVText = sText;
    i_VIdentifier = iIdentifier;
}

void CubeSelector::RegisterHorizontalSize(QDoubleSpinBox *pSinBox, QString sText, int iIdentifier)
{
    spin_HS = pSinBox;
    connect(spin_HS,SIGNAL(valueChanged(double)),
            this,SLOT(OnWidthSpinBox(double)));

    o_HorizontalText.setHtml(sText);
    s_HText = sText;
    i_HSIdentifier = iIdentifier;
}

void CubeSelector::RegisterVerticalSize(QDoubleSpinBox *pSpinBox, QString sText, int iIdentifier)
{
    spin_VS = pSpinBox;
    connect(spin_VS,SIGNAL(valueChanged(double)),
            this,SLOT(OnHeightSpinBox(double)));

    o_VerticalTextText.setHtml(sText);
    s_VText = sText;
    i_VSIdentifier = iIdentifier;
}

void CubeSelector::RegistrationComplete()
{
    QGraphicsRectItem* pParent = o_Scene.addRect(QRectF(0,0,
                                                 p_Model->GetValue(Parent,i_HSIdentifier),
                                                 p_Model->GetValue(Parent,i_VSIdentifier)));

    p_View->setMaximumSize( p_Model->GetValue(Parent,i_HSIdentifier),
                            p_Model->GetValue(Parent,i_VSIdentifier));

    p_View->setFixedSize(QSize(p_Model->GetValue(Parent,i_HSIdentifier),
                               p_Model->GetValue(Parent,i_VSIdentifier)));

    p_Rect = new GraphicsItem(QRectF(p_Model->GetValue(Child,i_HIdentifier),
                              p_Model->GetValue(Child,i_VIdentifier),
                              p_Model->GetValue(Child,i_HSIdentifier),
                              p_Model->GetValue(Child,i_VSIdentifier)),
                              pParent,this);
    p_Rect->setFlags(QGraphicsItem::ItemIsMovable | QGraphicsItem::ItemIsSelectable |QGraphicsItem::ItemSendsGeometryChanges);

    o_Scene.addItem(&o_HorizontalText);
    o_Scene.addItem(&o_VerticalTextText);
    o_Scene.addItem(&o_HPointText);
    o_Scene.addItem(&o_VPointText);
    ResetText();
}

void CubeSelector::OnXSpinBox(double dValue)
{
    if(!p_Rect)
        return;
    if(b_Lock)
        return;


    QRectF oRect = p_Rect->sceneBoundingRect();
    QPointF oPoint = p_Rect->pos();

    double dWidthCnage = dValue - oRect.x();

    oPoint.setX(dValue);
    //p_Rect->setPos(oPoint);
    oRect.setX(dValue);

    p_Rect->moveBy(dWidthCnage,0);
   // oRect.setWidth(oRect.width() /*+ dWidthCnage*/);

    ResetText();
    //p_Rect->setRect(oRect);
    p_Model->_OnChange(i_HIdentifier,dValue);
    emit NotifyRectMoved(oRect);
}

void CubeSelector::OnYSpinBox(double dValue)
{
    if(!p_Rect)
        return;

    if(b_Lock)
        return;

    QRectF oRect = p_Rect->sceneBoundingRect();
    double dWidthCnage = dValue - oRect.y();
    oRect.setY(dValue);

    //oRect.setHeight(oRect.height() + dWidthCnage);
   // p_Rect->setRect(oRect);

    p_Rect->moveBy(0,dWidthCnage);
    ResetText();
    p_Model->_OnChange(i_VIdentifier,dValue);
    emit NotifyRectMoved(oRect);
}

void CubeSelector::OnZSpinBox(double dValue)
{
    emit NotifyZChanged(dValue);
}

void CubeSelector::OnWidthSpinBox(double  dValue)
{
    if(!p_Rect)
        return;

    if(b_Lock)
        return;

    QRectF oRect = p_Rect->rect();
    oRect.setWidth(dValue);
    p_Rect->setRect(oRect);
    QSize oSize(oRect.height(),oRect.width());
    ResetText();
    p_Model->_OnChange(i_HSIdentifier,dValue);
    emit NotifyRectResized(oSize);

}

void CubeSelector::OnHeightSpinBox(double dValue)
{
    if(!p_Rect)
        return;

    if(b_Lock)
        return;

    QRectF oRect = p_Rect->rect();
    oRect.setHeight(dValue);
    p_Rect->setRect(oRect);
    QSize oSize(oRect.height(),oRect.width());
    ResetText();
    p_Model->_OnChange(i_VSIdentifier,dValue);
    emit NotifyRectResized(oSize);
}

void CubeSelector::OnDepthSpinBox(double dValue)
{
    emit NotifyDepthChanged(dValue);
}

void CubeSelector::ResetText()
{
    QRectF oRect = p_Rect->sceneBoundingRect();
    QPointF oSecond = oRect.topRight();
    QPointF oFirst = oRect.topLeft();
    QPointF oMiddle = oSecond + oFirst;

    o_HPointText.setPos(oFirst.x(),oFirst.y());
    o_HPointText.setHtml(QString("(%1,%2 <br/> &nbsp; %3,%4)").arg(s_PHText).arg(oFirst.x()).arg(s_PVText).arg(oFirst.y()));
    o_HorizontalText.setPos(oMiddle.x()/2,oMiddle.y()/2);

    QLineF oLine(oSecond,oFirst);
    o_HorizontalText.setHtml(QString("%1 : %2").arg(s_HText).arg(oLine.length()));

    oSecond = oRect.bottomLeft();
    oMiddle = oSecond + oFirst;

    o_VerticalTextText.setPos(oMiddle.x()/2, oMiddle.y()/2);

    QLineF oLineV(oSecond,oFirst);
    o_VerticalTextText.setHtml(QString("%1 : %2").arg(s_VText).arg(oLineV.length()));


}


